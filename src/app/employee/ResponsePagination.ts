import { Employee } from './Employee';

export class ResponsePagination{
    result: Employee[];
    total: number;
    page: number;
    returnedRecords: number;
}