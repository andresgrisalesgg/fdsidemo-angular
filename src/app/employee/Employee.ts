export class Employee{
    id: number;
    firstName: String;
    lastName: String;
    departmentName: String;
    position: String;
    telephone: String;
}