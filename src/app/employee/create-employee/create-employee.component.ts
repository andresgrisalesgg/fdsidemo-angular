import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeServiceService } from '../employee-service.service';
import { Employee } from '../Employee';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  employee: Employee = new Employee();
  constructor(private router: Router, private service: EmployeeServiceService) { }

  ngOnInit(): void {
  }

  create(){
    console.log(this.employee)
    this.service.create(this.employee)
    .subscribe(data => {
      alert("Employee created.");
    })
  }

}
