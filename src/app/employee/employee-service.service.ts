import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Employee } from './Employee';
import { ResponsePagination } from './ResponsePagination';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  constructor(private http: HttpClient) {

   }

   create(employee: Employee){

     return this.http.post<Employee>('https://fdsiapp-demo20.herokuapp.com/api/v1/employees', employee);
   }

   getAll(){
     return this.http.get<ResponsePagination>('https://fdsiapp-demo20.herokuapp.com/api/v1/employees');
   }
}
