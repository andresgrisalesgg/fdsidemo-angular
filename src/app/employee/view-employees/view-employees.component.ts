import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';
import { EmployeeServiceService } from '../employee-service.service';
import { Router } from '@angular/router';
import { ResponsePagination } from '../ResponsePagination';

@Component({
  selector: 'app-view-employees',
  templateUrl: './view-employees.component.html',
  styleUrls: ['./view-employees.component.css']
})
export class ViewEmployeesComponent implements OnInit {

  responsePagination: ResponsePagination;
  employees: Employee[];
  constructor(private router: Router, private employeeService: EmployeeServiceService) { }

  ngOnInit(): void {
    this.employeeService.getAll().subscribe(data => {
      console.log(data);
      this.responsePagination = data;

      this.employees = this.responsePagination.result;
    })
  }

}
