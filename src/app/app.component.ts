import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fsidemo';

  constructor(private router: Router){

  }

  createEmployee(){
    this.router.navigate(['create-employee'])
  }

  viewEmployees(){
    this.router.navigate(['view-employees'])
  }
}
