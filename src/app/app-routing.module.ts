import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateEmployeeComponent } from './employee/create-employee/create-employee.component';
import { ViewEmployeesComponent } from './employee/view-employees/view-employees.component';


const routes: Routes = [
  {path:"create-employee", component:CreateEmployeeComponent},
  {path:"view-employees", component: ViewEmployeesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
